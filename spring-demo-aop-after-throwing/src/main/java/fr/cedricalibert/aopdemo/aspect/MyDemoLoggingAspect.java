package fr.cedricalibert.aopdemo.aspect;

import java.util.Iterator;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.cedricalibert.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {

	@AfterThrowing(
				pointcut="execution(* fr.cedricalibert.aopdemo.dao.AccountDAO.findAccounts(..))",
				throwing="e"
			)
	public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable e) {
		//display the methods signature
		String methodSign = joinPoint.getSignature().toShortString();
						
		System.out.println("@AfterThrowing : Method : "+methodSign);
		
		System.out.println("The exeption is : "+e);
	}
	
	@AfterReturning(
			pointcut="execution(* fr.cedricalibert.aopdemo.dao.AccountDAO.findAccounts(..))",
			returning="result"
			)
	public void afterReturningFindAccountsAdvice(JoinPoint joinPoint, List<Account> result) {
		//display the methods signature
		String methodSign = joinPoint.getSignature().toShortString();
				
		System.out.println("@AfterReturning : Method : "+methodSign);
		
		System.out.println("Result is : "+ result);
		
		//modify the data post-process
		convertAccountsNamesToUpperCase(result);
		
		//printout the result after modify
		System.out.println("Result after modify is : "+ result);
		
		
	}
	
	private void convertAccountsNamesToUpperCase(List<Account> result) {
		for(Account account : result) {
			String toUpperName = account.getName().toUpperCase();
			account.setName(toUpperName);
		}
		
	}

	@Before("fr.cedricalibert.aopdemo.aspect.AopExpressions.forDaoPackageExceptSettersAndGetters()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint) {
		System.out.println("\n======> Executing @Before advice on addAccount");
		
		//display the methods signature
		MethodSignature methodSign = (MethodSignature) joinPoint.getSignature();
		
		System.out.println("Method : "+methodSign);
		
		//display method argument
		Object[] args = joinPoint.getArgs();
		
		for (Object arg : args) {
			System.out.println(arg);
			
			if(arg instanceof Account) {
				//downcast and print Account specific stuff
				Account account = (Account) arg;
				System.out.println("Account Name : "+account.getName());
				System.out.println("Account Level : "+account.getLevel());
			}
		}
		
	}
	
}
